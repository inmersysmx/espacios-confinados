﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectOneButton : MonoBehaviour
{

    public buttonStateChange[] buttons;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeObject(int index)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            if (index == i)
                buttons[i].ButtonIsSelected();
            else
                buttons[i].ButtonIsUnselected();
        }
    }
}
