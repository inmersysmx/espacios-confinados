﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmpiezaHablando : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Presentacion");
    }

    IEnumerator Presentacion()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Animator>().SetTrigger("habla");
    }
}
