﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RevisionSilos : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool desviacion;
    public bool oxido2;
    public bool ganchoAbierto;
    public bool dano;
    public bool quemadura2;
    public bool etiqueta2;
    public bool insidcadorRojo;
    public UnityEvent terminado;
    public UnityEvent faltante;
    public Image imageOxido;
    public Image imageQuemadura;
    public Image imageGancho;
    public Image imageDesviacion;
    public Image imageEtiqueta;
    public Image imageDano;
    public Sprite spriteDone;

    public void Desviacion()
    {
        GameDataManager.instance.desviacion = true;
        imageDesviacion.gameObject.SetActive(true);
        CheckSilos();
    }

    public void Oxido()
    {
        GameDataManager.instance.oxido2 = true;
        imageOxido.gameObject.SetActive(true);
        CheckSilos();
    }

    public void GanchoAbierto()
    {
        GameDataManager.instance.ganchoAbierto = true;
        imageGancho.gameObject.SetActive(true);
        CheckSilos();
    }

    public void Dano()
    {
        GameDataManager.instance.dano = true;
        imageDano.gameObject.SetActive(true);
        CheckSilos();
    }

    public void Quemadura()
    {
        GameDataManager.instance.quemadura2 = true;
        imageQuemadura.gameObject.SetActive(true);
        CheckSilos();
    }

    public void Etiqueta()
    {
        GameDataManager.instance.etiqueta2 = true;
        imageEtiqueta.gameObject.SetActive(true);
        CheckSilos();
    }

    public void IndicadorRojo()
    {
        GameDataManager.instance.insidcadorRojo = true;
    }

    public void CheckSilos()
    {
        if (!GameDataManager.instance.oxido2 || !GameDataManager.instance.quemadura2 || !GameDataManager.instance.ganchoAbierto || !GameDataManager.instance.desviacion ||
            !GameDataManager.instance.etiqueta2 || !GameDataManager.instance.dano)
            return;
        if (!GameDataManager.instance.quemadura1 || !GameDataManager.instance.rotura || !GameDataManager.instance.oxido1 || !GameDataManager.instance.brocheRoto ||
           !GameDataManager.instance.etiqueta1 || !GameDataManager.instance.ajuste)
            StartCoroutine(SiguientePanel(faltante));
        else
            StartCoroutine(SiguientePanel(terminado));
    }

    IEnumerator SiguientePanel(UnityEvent evento)
    {
        yield return new WaitForSeconds(1);
        evento.Invoke();
    }
}
