﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ManagerScene2 : MonoBehaviour
{
    public Transform player;
    public GameObject[] panels;
    public Transform[] positionsToMove;
    public Transform[] positionAccident;
    public GameObject[] botonesContinuar;
    public GameObject[] floorCorrectHook;
    public GameObject[] instructionsHook;
    public int startPosition;
    public GameObject hook;
    private float transitionSpeed = 2;

    public Material materialEsferaInvertida;
    public GameObject sangre;

    [Header("Accident")]
    private Animator animatorAccident;
    public Animator[] accidentAnimators;


    [Header("Audios")]
    public AudioSource audioSourceVoice;
    public AudioSource audioPreAccident;
    public AudioClip[] clipPreAccident;
    public AudioSource audioAccident;
    public AudioClip[] clipAccident;
    public AudioClip[] audiosClipsVoice;
    public AudioSource hitTheGround;


    public bool correctHook;
    public static ManagerScene2 instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        startPosition = GameDataManager.instance.locationNumber;
        player.position = positionsToMove[startPosition].position;
        player.rotation = positionsToMove[startPosition].rotation;
        animatorAccident = accidentAnimators[startPosition];
        Hook.instance.buttonContinue = botonesContinuar[startPosition];
        Hook.instance.instructionHook = instructionsHook[startPosition];
        correctHook = false;
        Invoke("StartAudio", 5f);

        audioPreAccident.clip = clipPreAccident[startPosition];

        //DataManager.instance.anclajeCorrecto = true;

        audioAccident.clip = clipAccident[startPosition];

        if (startPosition == 1)
            hook.SetActive(false);
    }



    public void StartAudio()
    {
        LaserJC.instance.canPressButton = (startPosition == 1);
        if (GameDataManager.instance.locationNumber != 1)
        {
            audioSourceVoice.clip = audiosClipsVoice[0];
            audioSourceVoice.Play();
        }
        StartCoroutine(ScalePanelUp(startPosition));

        if (startPosition != 1)
        {
            Hook.instance.InstructionsHook();
            Hook.instance.canHook = true;
        }

    }




    IEnumerator ScalePanelUp(int index)
    {
        panels[index].transform.localScale = new Vector3(0, 0, 0);
        panels[index].SetActive(true);
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime * transitionSpeed;
            panels[index].transform.localScale = new Vector3(t, t, t);
            yield return null;
        }
    }

    public void ScaleDown()
    {
        GameDataManager.instance.anchoreLine = correctHook;
        GameDataManager.instance.CheckEverythingCorrect();
        StartCoroutine(ScalePanelDown(startPosition));
    }

    IEnumerator ScalePanelDown(int index)
    {
        LaserJC.instance.canPressButton = false;
        hook.SetActive(false);

        GameDataManager.instance.anchoreLine = correctHook;

        if (GameDataManager.instance.CheckEverythingCorrect())
        {
            floorCorrectHook[startPosition].SetActive(true);
            Invoke("PlayAudioCorrect", 14f);
        }
            


        float t = 1;
        while (t > 0)
        {
            t -= Time.deltaTime * transitionSpeed;
            panels[index].transform.localScale = new Vector3(t, t, t);
            yield return null;
        }
        panels[index].transform.localScale = new Vector3(0, 0, 0);
        panels[index].SetActive(false);

        t = 0;
        while (t < 1)
        {
            t += Time.deltaTime*0.5f;
            yield return null;
        }

        StartCoroutine(MovePlayerToAccident());

    }

    IEnumerator MovePlayerToAccident()
    {
        Color colorEsfera = Color.black;
        colorEsfera.a = 0;
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime;
            colorEsfera.a = t;
            materialEsferaInvertida.color = colorEsfera;
            yield return null;
        }

        
        yield return StartCoroutine(PlayAudioAndWait(audioPreAccident));

        player.position = positionAccident[startPosition].position;

        animatorAccident.Play("Up");
        
        t = 1;
        while (t > 0)
        {
            t -= Time.deltaTime;
            colorEsfera.a = t;
            materialEsferaInvertida.color = colorEsfera;
            yield return null;
        }

        animatorAccident.Play("Fall");
        audioAccident.Play();

        //Invoke("MoveToNextScene", 7f);
    }




    IEnumerator PlayAudioAndWait(AudioSource audioSource)
    {
        audioSource.Play();

        while (!audioSource.isPlaying)
            yield return null;

        yield return null;

        while (audioSource.isPlaying)
            yield return null;
    }

    public void ActivateBlood()
    {
        hitTheGround.Play();
        sangre.SetActive(true);
        Invoke("PlayAudioError", 3f);
    }

    public void PlayAudioError()
    {
        audioSourceVoice.clip = audiosClipsVoice[2];
        StartCoroutine(PlayAudioWaitForScene());
    }

    public void PlayAudioCorrect()
    {
        audioSourceVoice.clip = audiosClipsVoice[1];
        StartCoroutine(PlayAudioWaitForScene());
    }

    IEnumerator PlayAudioWaitForScene()
    {
        yield return PlayAudioAndWait( audioSourceVoice);

        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime;
            yield return null;

        }

        MoveToNextScene();
    }

    public void MoveToNextScene()
    {
        SceneManager.LoadScene(2);
    }

}
