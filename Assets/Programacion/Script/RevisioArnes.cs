﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RevisioArnes : MonoBehaviour
{
    public int apretar = 0;

    public UnityEvent terminado;
    public UnityEvent faltante;
    public Image imageQuemadura;
    public Image imageRotura;
    public Image imageOxido;
    public Image imageBroche;
    public Image imageEtiqueta;
    public Sprite spriteDone;

    public void Quemadura()
    {
        GameDataManager.instance.quemadura1 = true;
        imageQuemadura.gameObject.SetActive(true);
        CheckArnes();
    }

    public void Rotura()
    {
        GameDataManager.instance.rotura = true;
        imageRotura.gameObject.SetActive(true);
        CheckArnes();
    }

    public void Oxido()
    {
        GameDataManager.instance.oxido1 = true;
        imageOxido.gameObject.SetActive(true);
        CheckArnes();
    }

    public void Broche()
    {
        GameDataManager.instance.brocheRoto = true;
        imageBroche.gameObject.SetActive(true);
        CheckArnes();
    }

    public void Etiqueta()
    {
        GameDataManager.instance.etiqueta1 = true;
        imageEtiqueta.gameObject.SetActive(true);
        CheckArnes();
    }

    public void Ajuste()
    {
        apretar++;

        if(apretar == 3)
        {
            GameDataManager.instance.ajuste = true;
            CheckAjustes();
            apretar = 0;
        }
            
    }

    public void CheckArnes()
    {
        if (!GameDataManager.instance.quemadura1 || !GameDataManager.instance.rotura || !GameDataManager.instance.oxido1 || !GameDataManager.instance.brocheRoto ||
           !GameDataManager.instance.etiqueta1)
            return;
        StartCoroutine(PasaAjuste());
    }

    IEnumerator PasaAjuste()
    {
        yield return new WaitForSeconds(1);
        PanelManager.instance.ChangeToNewPanel(11);
    }

    public void CheckAjustes()
    {
        if (!GameDataManager.instance.quemadura1 || !GameDataManager.instance.rotura || !GameDataManager.instance.oxido1 || !GameDataManager.instance.brocheRoto ||
           !GameDataManager.instance.etiqueta1 || !GameDataManager.instance.ajuste)
            return;
        if (!GameDataManager.instance.oxido2 || !GameDataManager.instance.quemadura2 || !GameDataManager.instance.ganchoAbierto || !GameDataManager.instance.desviacion ||
            !GameDataManager.instance.etiqueta2 || !GameDataManager.instance.dano)
            StartCoroutine(SiguientePanel(faltante));
        else
            StartCoroutine(SiguientePanel(terminado));
    }

    IEnumerator SiguientePanel(UnityEvent evento)
    {
        yield return new WaitForSeconds(1);
        evento.Invoke();
    }
}
