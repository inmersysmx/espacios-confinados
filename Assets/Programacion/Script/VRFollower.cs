using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VRFollower : MonoBehaviour
{

	public Transform camaraUsuario;
	public Vector3 offset;
	public float speed;
	public bool lookAt;
    public bool verticalMovement;
	void Start ()
	{
        Invoke("removerColision", 1.0f);
	}

    void removerColision() {
        MeshCollider mc = this.gameObject.GetComponent<MeshCollider>();
        if (mc != null)
            mc.enabled = false;
    }
	

	void Update ()
	{
        Vector3 displacement = (camaraUsuario.transform.right * offset.x) + (camaraUsuario.transform.up * offset.y) + (camaraUsuario.transform.forward * offset.z);
        
        if (!verticalMovement)
            displacement = new Vector3(displacement.x, offset.y, displacement.z);

        this.transform.position = Vector3.Lerp(this.transform.position, camaraUsuario.transform.position + displacement, Time.deltaTime * speed);
		if (lookAt){
            this.transform.LookAt(camaraUsuario, Vector3.up);
            this.transform.Rotate(Vector3.up * 180);
        }
    }

	void FixedUpdate ()
	{

	}
}




