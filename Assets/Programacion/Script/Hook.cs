﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hook : MonoBehaviour
{

    public AudioSource audioSourceHooked;

    public bool canHook;
    public bool isHooked;
    public bool isOverHook;
    public bool controllerIsOverHook;

    public Transform hookParent;
    public Transform currentHook;
    public MeshRenderer hookGhost;

    public GameObject buttonContinue;
    public GameObject instructionHook;

    private MeshRenderer hookMesh;

    public static Hook instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        canHook = false;
        hookMesh = GetComponent<MeshRenderer>();

    }

    private void Update()
    {
        hookMesh.enabled = canHook;

        if (!isHooked)
        {
            if (canHook && isOverHook && (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)))
            {
                audioSourceHooked.Play();
                isHooked = true;
                this.transform.SetParent(currentHook);
                currentHook.GetComponent<MeshRenderer>().enabled = false;
                buttonContinue.SetActive(true);
                LaserJC.instance.canPressButton = true;
            }
        }
        else
        {
            if (canHook && controllerIsOverHook && (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)))
            {
                this.transform.SetParent(hookParent);
                this.transform.localRotation = new Quaternion(0, 0, 0, 0);
                this.transform.localPosition = Vector3.zero;
                hookGhost.enabled = false;
                isHooked = false;
                buttonContinue.SetActive(false);
                LaserJC.instance.canPressButton = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (canHook)
        {
            if (!isHooked)
            {
                MeshRenderer meshR = other.GetComponent<MeshRenderer>();

                if (meshR != null)
                {
                    meshR.enabled = true;
                }

                if (other.tag == "CorrectGrabPoint")
                {
                    ManagerScene2.instance.correctHook = true;
                    isOverHook = true;
                    currentHook = other.transform;
                }
                else if (other.tag == "IncorrectGrabPoint")
                {
                    ManagerScene2.instance.correctHook = false;
                    isOverHook = true;
                    currentHook = other.transform;
                }
                else
                {
                    isOverHook = false;
                    currentHook = null;
                }
            }
            else
            {
                if (other.tag == "HookParent")
                {
                    controllerIsOverHook = true;
                    hookGhost.enabled = true;
                }
                else
                {
                    Debug.Log("El tag " + other.tag);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (!isHooked)
        {
            MeshRenderer meshR = other.GetComponent<MeshRenderer>();

            if (meshR != null)
            {
                meshR.enabled = false;
            }

            isOverHook = false;
        }
        else
        {
            controllerIsOverHook = false;
            hookGhost.enabled = false;
        }

    }

    public void InstructionsHook()
    {
        StartCoroutine(CheckIfWasHooked());
    }

    IEnumerator CheckIfWasHooked()
    {
        float t = 0;
        while(t<10 && !isHooked)
        {
            t += Time.deltaTime;
            yield return null;
        }

        if (t >= 10)
            instructionHook.SetActive(true);
    }
}
