﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class buttonStateChange : MonoBehaviour
{
    //private Collider colliderButton;
    //private Image buttonImage;
    //public Sprite disableButton;
    //public Sprite enableButton;
    //public Sprite selectedButton;
    //public Sprite pressButton;
    public GameObject letrero;
    public ScaleObject scaleObject;

    //public bool isEnable;
    public bool isSelected;
    public bool isOverButton;
    private void Start()
    {
        scaleObject = GetComponent<ScaleObject>();
        isSelected = false;
        //colliderButton = GetComponent<Collider>();
        //buttonImage = GetComponent<Image>();
        //isEnable = true;
    }

    //public void ChangeToEnable()
    //{
    //    buttonImage.sprite = enableButton;
    //    colliderButton.enabled = true;
    //    isEnable = true;
    //}

    //public void ChangeToDisable()
    //{
    //    buttonImage.sprite = disableButton;
    //    colliderButton.enabled = false;
    //    isEnable = false;
    //    isOverButton = false;
    //}

    public void OverButton()
    {
        //buttonImage.sprite = selectedButton;
        letrero.SetActive(true);
        isOverButton = true;
    }

    public void NotOverButton()
    {
        if(!isSelected)
            letrero.SetActive(false);

        isOverButton = false;

        //buttonImage.sprite = enableButton;
    }

    public void ButtonIsSelected()
    {
        isSelected = true;
        scaleObject.ScaleUp();
        //isEnable = false;
        //buttonImage.sprite = pressButton;
        //colliderButton.enabled = false;
    }

    public void ButtonIsUnselected()
    {
        isSelected = false;
        scaleObject.ScaleDown();
        letrero.SetActive(false);
        //isEnable = false;
        //buttonImage.sprite = pressButton;
        //colliderButton.enabled = false;
    }


}
