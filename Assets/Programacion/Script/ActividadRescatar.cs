﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActividadRescatar : Singleton<ActividadRescatar>
{
    public Transform control1;
    public Transform control2;
    public GameObject targetManivela;
    public GameObject proxy;
    public ManivelaLookAt manivelaLookAtInterior;
    public ManivelaLookAt manivelaLookAtExterior;
    public int avance;
    public GameObject prevTrigger;

    bool controlesJuntosFirstTime;

    private void Update()
    {
        if(/*PanelManager.instance.panels[22].activeSelf*/ ManagerScene1.instance.curScenario != null)
        {
            if(AdministradorEscenas.instance.rescateActivo)
            {
                if (Vector3.Distance(control1.position, control2.position) < 0.1f)
                {
                    targetManivela.SetActive(true);
                    proxy.SetActive(true);
                    if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                        manivelaLookAtInterior.enabled = true;
                    else
                        manivelaLookAtExterior.enabled = true;
                }
                else
                {
                    targetManivela.SetActive(false);
                    proxy.SetActive(false);
                    if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                        manivelaLookAtInterior.enabled = false;
                    else
                        manivelaLookAtExterior.enabled = false;
                }
            }
            else
            {
                if(Vector3.Distance(control1.position, control2.position) < 0.1f)
                {
                    if(PanelManager.instance.panels[24].activeSelf && !controlesJuntosFirstTime)
                    {
                        controlesJuntosFirstTime = true;
                        AdministradorEscenas.instance.StartRescate();
                        targetManivela.SetActive(true);
                    }
                }
                else
                {
                    targetManivela.SetActive(false);
                    proxy.SetActive(false);
                    if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                        manivelaLookAtInterior.enabled = false;
                    else
                        manivelaLookAtExterior.enabled = false;
                }
            }
        }
    }
}
