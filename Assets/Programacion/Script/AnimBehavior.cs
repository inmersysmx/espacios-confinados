﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimBehavior : MonoBehaviour
{
    public void DeactivateAnim()
    {
        AdministradorEscenas.instance.playerAnimator.Play("Idle");
        AdministradorEscenas.instance.playerAnimator.enabled = false;
        //ManagerScene1.instance.ovrPlaye.GetComponent<OVRPlayerController>().enabled = true;
    }
}
