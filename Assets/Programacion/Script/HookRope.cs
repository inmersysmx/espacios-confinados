﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookRope : MonoBehaviour
{
    public AudioSource audioSourceHooked;

    //public bool canHook;
    //public bool isHooked;
    //public bool isOverHook;
    public bool controllerIsOverHook;

    public Transform hookParent;
    public GameObject hookRopeGhost;
    //public GameObject planeFall;
    public GameObject rope;


    //public GameObject buttonContinue;
    //public GameObject instructionHook;

    public static HookRope instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        //canHook = false;

    }

    private void Update()
    {
        if (controllerIsOverHook && (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger)))
        {
            this.transform.SetParent(hookParent);
            this.transform.localRotation = hookRopeGhost.transform.localRotation;
            this.transform.localPosition = hookRopeGhost.transform.localPosition;
            hookRopeGhost.SetActive(false);
            LaserJC.instance.canPressButton = false;
            //isHooked = true;
            //LaserJC.instance.canPressButton = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "HookParent")
        {
            controllerIsOverHook = true;
            hookRopeGhost.SetActive(true);
        }

        if(other.tag == "RopeTest")
        {
            ManagerScene2.instance.correctHook = true;
            ManagerScene2.instance.ScaleDown();
            rope.SetActive(false);
            this.gameObject.SetActive(false);
            audioSourceHooked.Play();
            //planeFall.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "HookParent")
        {
            controllerIsOverHook = false;
            hookRopeGhost.SetActive(false);
        }

    }

    //public void InstructionsHook()
    //{
    //    StartCoroutine(CheckIfWasHooked());
    //}

    //IEnumerator CheckIfWasHooked()
    //{
    //    float t = 0;
    //    while (t < 10 && !isHooked)
    //    {
    //        t += Time.deltaTime;
    //        yield return null;
    //    }

    //    if (t >= 10)
    //        instructionHook.SetActive(true);
    //}
}
