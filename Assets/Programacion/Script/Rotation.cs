﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rotation : MonoBehaviour
{

    public Transform objectToRotate;
    public float speed =1;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RotateRight()
    {
        StartCoroutine(ObjectRotation(-1));
    }

    public void RotateLeft()
    {
        StartCoroutine(ObjectRotation(1));
    }

    IEnumerator ObjectRotation(int direction)
    {
        yield return null;
        Button currentButton = LaserJC.instance.lastButton;

        yield return null;

        while ((currentButton == LaserJC.instance.lastButton) && OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger))
        {
            objectToRotate.Rotate(0, direction * speed, 0);
            yield return null;
        }

    }
}
