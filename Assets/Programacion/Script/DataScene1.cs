﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataScene1 : MonoBehaviour
{

    public Text textID;
    public Text textName;
    public int location;

    public GameObject[] ObjectsSilos;
    public GameObject[] objectsPipa;
    public GameObject[] objectsmontacargas;

    void Start()
    {
        //CSVManager.CreateReport();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StoreInfo()
    {
        GameDataManager.instance.id = textID.text;
        GameDataManager.instance.userName = textName.text;
    }

    public void SelectLocationAtRandom()
    {
        print("esto sucede al firma");
        location = Random.Range(0, 3);
        GameDataManager.instance.locationNumber = location;
        PanelManager.instance.ChangeToNewPanel(5 + location);
        Debug.Log("Location " + location);

        GameObject[] currentObjects = ObjectsSilos;

        switch (location)
        {
            case 0:
                currentObjects = ObjectsSilos;
                //GameDataManager.instance.location = "Silos";
                break;
            case 1:
                currentObjects = objectsPipa;
                //GameDataManager.instance.location = "Pipa";
                break;
            case 2:
                currentObjects = objectsmontacargas;
                //GameDataManager.instance.location = "Montacargas";
                break;
        }

        for (int i = 0; i < currentObjects.Length; i++)
        {
            currentObjects[i].SetActive(true);
        }
    }
}
