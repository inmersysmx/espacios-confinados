﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMancha : MonoBehaviour
{
    Mancha curMancha;

    private void Start()
    {
        curMancha = transform.parent.GetComponent<Mancha>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Cepillo")
        {
            curMancha.numeroTalladas++;
            print(curMancha.numeroTalladas);
            if (curMancha.numeroTalladas >= 10)
            {
                AdministradorEscenas.instance.manchaslimpiadas++;
                Destroy(curMancha.gameObject);
            }
        }
    }
}
