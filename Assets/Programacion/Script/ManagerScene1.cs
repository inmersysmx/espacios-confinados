﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ManagerScene1 : MonoBehaviour
{


    public LaserJC laserJc;
    public CanvasGroup SplashCanvas;
    public CanvasGroup SplashCorazones;
    public GameObject scenarioInterior;
    public GameObject scenarioExterior;
    public GameObject esferaInvertida;
    public GameObject ovrPlaye;
    public GameObject medidorAtmosferico;
    public GameObject modeloControlOculus;
    public GameObject modeloControlOculusIz;
    public GameObject modeloMedidorAtmosferico;
    public GameObject modeloCepillo;
    public GameObject modeloCepilloIz;
    public GameObject pisoExterior;
    public GameObject vigilanteInterior;
    public GameObject vigilanteExterior;

    [Header("Animators")]
    public Animator splashAnim;
    public Animator splashAnimBtn;


    [Header("Audios")]
    public AudioSource audioSourceVoice;
    public AudioClip[] audiosClipsVoice;
    public AudioSource audioSourceAmbiental;
    public AudioClip[] audiosClipsAmbiental;
    public AudioSource audioSourceMedidorAtmosfera;
    public AudioSource audioSourceMedidorPuerta;
    public AudioSource audioSourceMedidorGas;
    public AudioSource audioSourcePierdeCorazon;
    public AudioSource audioSourceReloj;
    public AudioSource audioSourceError;
    public AudioSource audioSourceAcierto;
    public Transform curScenario;

    public static ManagerScene1 instance;

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            if (modeloMedidorAtmosferico.activeSelf || modeloCepillo.activeSelf)
                modeloControlOculus.SetActive(false);
            if (modeloCepilloIz.activeSelf)
                modeloControlOculusIz.SetActive(false);
        }
    }

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        GameDataManager.instance.ResetAllValues();
        StartCoroutine(PlayIntro());
    }

    //Splash and trigger instructions
    IEnumerator PlayIntro()
    {
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime*3;
            yield return null;
        }

        audioSourceVoice.clip = audiosClipsVoice[0];

        yield return StartCoroutine(PlayAudioAndWait());
       
        splashAnim.Play("ControlTutorial_Intro");
        splashAnimBtn.Play("ControlTutorial_Intro");
        //while (!Input.GetMouseButtonDown(0))
        //{
        //    yield return null;
        //}

        while (!OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger))
        {
            yield return null;
        }

        splashAnim.Play("ControlTutorial_Salida");

        t = 0;
        while (t < 1)
        {
            t += Time.deltaTime*2;
            yield return null;
        }


        t = 1;
        while (t >0)
        {
            t -= Time.deltaTime;
            SplashCanvas.alpha = t;
            yield return null;
        }

        SplashCanvas.gameObject.SetActive(false);
        RandomScenario();
        PanelManager.instance.OpenFirstPanel();

        laserJc.canPressButton = true;
    }

    void RandomScenario()
    {
        int scenario = Random.Range(0, 2);
        scenarioInterior.SetActive(scenario == 1 ? true : false);
        scenarioExterior.SetActive(scenario == 0 ? true : false);
        esferaInvertida.SetActive(false);
        if(scenarioInterior.activeSelf)
        {
            pisoExterior.SetActive(false);
        }
        curScenario = scenario == 1 ? scenarioInterior.transform : scenarioExterior.transform;
    }

    IEnumerator PlayAudioAndWait()
    {
        audioSourceVoice.Play();

        while (!audioSourceVoice.isPlaying)
            yield return null;

        yield return null;

        while (audioSourceVoice.isPlaying)
            yield return null;
    }

    public void ChangeAmbientalAudio(int index)
    {
        StartCoroutine(changeAmbientalAudio(index));
    }

    IEnumerator changeAmbientalAudio(int index)
    {
        while(audioSourceAmbiental.volume > 0)
        {
            audioSourceAmbiental.volume -= 0.1f;
            yield return null;
        }
        audioSourceAmbiental.clip = audiosClipsAmbiental[index];
        audioSourceAmbiental.Play();
        while(audioSourceAmbiental.volume < 0.3f)
        {
            audioSourceAmbiental.volume += 0.1f;
            yield return null;
        }
    }

    public void SelectArea()
    {
        PanelManager.instance.ChangeToNewPanel(Random.Range(5,8));
    }

    public void ChangeAndPlayVoice(int index)
    {
        audioSourceVoice.clip = audiosClipsVoice[index];

        audioSourceVoice.Play();
    }

    public void MoveToNextScene()
    {
        SceneManager.LoadScene(1);
    }

    public void EntradaCorazones()
    {
        StartCoroutine(entradaCorazones());
    }

    IEnumerator entradaCorazones()
    {
        SplashCorazones.gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        SplashCorazones.GetComponentInChildren<Animator>().enabled = true;
        yield return new WaitForSeconds(5);
        SplashCorazones.GetComponentInChildren<Animator>().Play("Sube");
        yield return new WaitForSeconds(4);
        AdministradorEscenas.instance.faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        SplashCorazones.GetComponentInChildren<Animator>().enabled = false;
        if(curScenario.name == "Escenario Interior")
        {
            PanelManager.instance.ChangeToNewPanel(8);
        }
        else
        {
            PanelManager.instance.ChangeToNewPanel(7);
        }
        ovrPlaye.transform.eulerAngles = new Vector3(0, 180, 0);
    }

    public void VigilantePersistente(Transform vigilante)
    {
        StartCoroutine(vigilantePersistente(vigilante));
    }

    public float smoothing = 5f;

    IEnumerator vigilantePersistente(Transform vigilante)
    {
        vigilante.SetParent(curScenario);
        if(curScenario.name == "Escenario Interior")
            vigilante.transform.localEulerAngles = new Vector3(0, 180, 0);
        else
            vigilante.transform.localEulerAngles = new Vector3(0, -90, 0);
        Transform origin = curScenario.Find("OriginVigilante").transform;
        while (Vector3.Distance(vigilante.position, origin.position) > 0.01f)
        {
            vigilante.transform.position = Vector3.Lerp(vigilante.transform.position, origin.position, smoothing * Time.deltaTime);
            yield return null;
        }
        vigilante.gameObject.SetActive(false);
        if (curScenario.name == "Escenario Interior")
            vigilanteInterior.SetActive(true);
        else
            vigilanteExterior.SetActive(true);
    }

    bool resetedRotation;

    public void RotationOVRReset()
    {
        StartCoroutine(rotationOVRReset());
    }

    IEnumerator rotationOVRReset()
    {
        if(!resetedRotation)
        {
            resetedRotation = true;
            AdministradorEscenas.instance.faderAnimator.SetTrigger("fade");
            yield return new WaitForSeconds(1);
        }
        ovrPlaye.transform.eulerAngles = Vector3.zero;
    }

    public void EntradaMedidorAtmosferico()
    {
        PanelManager.instance.medicionAtmosferica = true;
        StartCoroutine(entradaMedidorAtmosferico());
    }

    IEnumerator entradaMedidorAtmosferico()
    {
        yield return new WaitForSeconds(2);
        PanelManager.instance.medicionAtmosferica = false;
        medidorAtmosferico.SetActive(true);
        modeloControlOculus.SetActive(false);
        modeloMedidorAtmosferico.SetActive(true);
        AdministradorEscenas.instance.CargaEscenaConFade(3);
        PanelManager.instance.ChangeToNewPanel(18);
    }
}
