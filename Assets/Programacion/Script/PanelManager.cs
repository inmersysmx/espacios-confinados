﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;

public class PanelManager : MonoBehaviour
{
    public int currentScene;
    public GameObject[] panels;
    public int currentPanelIndex;
    public bool accidente1;
    public bool accidente2;
    public bool medicionAtmosferica;
    public bool limpiando;

    public static PanelManager instance;

    public float transitionSpeed = 2;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        for(int i = 0; i<panels.Length;i++)
            panels[i].SetActive(false);
    }

    public void OpenFirstPanel()
    {
        StartCoroutine(ScalePanelUp(0));
    }

    IEnumerator ScalePanelUp(int index)
    {
        LaserJC.instance.canPressButton = false;
        panels[index].transform.localScale = new Vector3(0,0,0);
        panels[index].SetActive(true);
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime*transitionSpeed;
            panels[index].transform.localScale = new Vector3(t, t, t);
            yield return null;
        }

        currentPanelIndex = index;
        LaserJC.instance.canPressButton = true;
        PlayVoice(index);
    }

    IEnumerator ScalePanelDown(int index)
    {
        LaserJC.instance.canPressButton = false;
        float t = 1;
        while (t >0)
        {
            t -= Time.deltaTime* transitionSpeed;
            panels[index].transform.localScale = new Vector3(t, t, t);
            yield return null;
        }
        panels[index].transform.localScale = new Vector3(0, 0, 0);
        panels[index].SetActive(false);
        LaserJC.instance.canPressButton = true;
    }

    public void ChangeToNewPanel(int index)
    {
        StartCoroutine(ChangeTo(index));
    }

    IEnumerator ChangeTo(int index)
    {

        yield return StartCoroutine(ScalePanelDown(currentPanelIndex));
        yield return null;
        yield return StartCoroutine(ScalePanelUp(index));
        currentPanelIndex = index;
    }


    public void PlayVoice(int index)
    {
        if (currentScene == 0)
        {
            if(index == 6)
            {
                if (accidente1)
                {
                    if (AdministradorEscenas.instance.bloqueoBoton && AdministradorEscenas.instance.bloqueoValvula)
                        ManagerScene1.instance.ChangeAndPlayVoice(23);
                    else
                        ManagerScene1.instance.ChangeAndPlayVoice(22);
                }
                else if (accidente2)
                    ManagerScene1.instance.ChangeAndPlayVoice(24);
                else if (medicionAtmosferica)
                    print("no suena nada");
                else if(limpiando)
                {
                    ManagerScene1.instance.ChangeAndPlayVoice(25);
                    limpiando = false;
                }
                else
                    ManagerScene1.instance.ChangeAndPlayVoice(index + 1);
            }
            else if (index < 16)
            {
                ManagerScene1.instance.ChangeAndPlayVoice(index + 1);
                if (index == 5 || index == 13)
                {
                    if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                        ManagerScene1.instance.vigilanteInterior.GetComponent<Animator>().SetTrigger("habla");
                    else
                        ManagerScene1.instance.vigilanteExterior.GetComponent<Animator>().SetTrigger("habla");
                }
            }
            else if(index > 16 && index < 22)
            {
                ManagerScene1.instance.ChangeAndPlayVoice(index);
                if (index == 20 || index == 21)
                {
                    if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                        ManagerScene1.instance.vigilanteInterior.GetComponent<Animator>().SetTrigger("habla");
                    else
                        ManagerScene1.instance.vigilanteExterior.GetComponent<Animator>().SetTrigger("habla");
                }
            }
            else if (index == 23)
            {
                ManagerScene1.instance.ChangeAndPlayVoice(index + 3);
            }
            else if (index > 25)
            {
                ManagerScene1.instance.ChangeAndPlayVoice(index + 1);
            }
        }
        else
        {
            if(index!= 0)
                ManagerScene3.instance.ChangeAndPlayVoice(index);
        }


    }
}
