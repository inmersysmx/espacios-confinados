﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LaserJC : MonoBehaviour
{
    private LineRenderer lineRenderer;
    //public Button button;

    public Button lastButton;
    public GameObject recticle;

    public bool canPressButton = true;
    public AudioSource hoverSound;
    public AudioSource selectSound;

    public buttonStateChange buttonState;
    public buttonStateChange lastButtonState;

    public static LaserJC instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.enabled = false;
        lineRenderer.useWorldSpace = true;
    }

    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, 100.0f))
        {
            recticle.transform.position = hit.point;
            recticle.SetActive(true);

            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, hit.point);
            lineRenderer.enabled = true;

            Button button = hit.transform.GetComponent<Button>();
            buttonState = hit.transform.GetComponent<buttonStateChange>();

            if (button != null && button.interactable)
            {
                //if (Input.GetKeyDown(KeyCode.A) && canPressButton)
                if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger) && canPressButton)
                {
                    button.onClick.Invoke();
                    StartCoroutine(WaitForNextPress());
                    selectSound.Play();
                }
                else if(lastButton != button)
                {
                    hoverSound.Play();

                    if (buttonState != null && button.interactable)
                        buttonState.OverButton();
                }

                lastButton = button;
                lastButtonState = buttonState;
            }
            else
            {
                if (lastButtonState != null)
                {
                    lastButtonState.NotOverButton();
                    lastButtonState = null;
                }

                lastButton = null;
            }

        }
        else
        {
            lastButton = null;
            lineRenderer.enabled = false;
            recticle.SetActive(false);
        }

        if (!canPressButton)
        {
            lineRenderer.enabled = false;
            recticle.SetActive(false);
            //lastButton = null;
        }
        
    }

    IEnumerator WaitForNextPress()
    {
        canPressButton = false;
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime * 4;
            yield return null;
        }

        canPressButton = true;
    }
}
