﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

public class EventAnimation : MonoBehaviour
{
    public GameObject candadoBoton;
    public GameObject candadoBloqueo;
    public Transform target;

    Vector3 initialRotation;

    private void Start()
    {
        initialRotation = transform.localEulerAngles;
    }

    public void ActivaCandadosVigilante()
    {
        print("candadosActivados");
        candadoBoton.SetActive(true);
        candadoBloqueo.SetActive(true);
    }

    public void ActivaLookAt()
    {
        

        var lookPos = target.position - transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = rotation;
    }

    public void DesactivaLookAt()
    {
        transform.localEulerAngles = initialRotation;
    }
}
