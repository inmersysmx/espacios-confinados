﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerScene3 : MonoBehaviour
{
    public AudioSource audioSourceVoice;

    public AudioClip[] audiosClipsVoice;
    public AudioClip audioIncorrecto;

    public GameObject panelCorrect;
    public GameObject panelIncorrect;

    public static ManagerScene3 instance;

    private void Awake()
    {
        instance = this;
    }


    void Start()
    {
        if (GameDataManager.instance.CheckEverythingCorrect())
        {
            panelCorrect.SetActive(true);
        }
        else
        {
            panelIncorrect.SetActive(true);
        }
            


        PanelManager.instance.OpenFirstPanel();
    }
    

    public void ChangeAndPlayVoice(int index)
    {
        audioSourceVoice.clip = audiosClipsVoice[index];

        audioSourceVoice.Play();
    }

    public void SaveInformation()
    {
        GameDataManager.instance.StoreInformation();
    }
}
