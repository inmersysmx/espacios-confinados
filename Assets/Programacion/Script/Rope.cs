﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    public Transform topRope;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.LookAt(topRope);
        float dist = Vector3.Distance(this.transform.position, topRope.position);
        this.transform.localScale = new Vector3(1, 1, dist);
    }
}
