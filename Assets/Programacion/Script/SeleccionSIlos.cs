﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeleccionSIlos : MonoBehaviour
{
    public int currentSiloSelected;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeSilosNumber(int index)
    {
        currentSiloSelected = index;
    }

    public void CheckIfCorrect()
    {
        //if (GameDataManager.instance.locationNumber==0)
        //{
        //    if (currentSiloSelected == 0)
        //        GameDataManager.instance.selectLifeline = true;
        //}
        //else
        //{
        //    if(currentSiloSelected>1)
        //        GameDataManager.instance.selectLifeline = true;
        //}

        switch (currentSiloSelected)
        {
            case 0:
                GameDataManager.instance.isTestedDano = false;
                GameDataManager.instance.isTestedQuemadura2 = false;
                GameDataManager.instance.isTestedEtiqueta2 = false;
                GameDataManager.instance.isTestedInsidcadorRojo = false;
                break;
            case 1:
                GameDataManager.instance.isTestedDano = true;
                GameDataManager.instance.isTestedQuemadura2 = true;
                GameDataManager.instance.isTestedEtiqueta2 = false;
                GameDataManager.instance.isTestedInsidcadorRojo = false;
                break;
            case 2:
                GameDataManager.instance.isTestedDano = true;
                GameDataManager.instance.isTestedQuemadura2 = true;
                GameDataManager.instance.isTestedEtiqueta2 = true;
                GameDataManager.instance.isTestedInsidcadorRojo = false;
                break;
            case 3:
                GameDataManager.instance.isTestedDano = false;
                GameDataManager.instance.isTestedQuemadura2 = false;
                GameDataManager.instance.isTestedEtiqueta2 = false;
                GameDataManager.instance.isTestedInsidcadorRojo = true;
                break;
        }
    }
}
