using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartUnmount : MonoBehaviour {
    public static RestartUnmount instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    
    void Start () {

	}
	
	void Update () {
		
	}

    private void OnEnable()
    {
        OVRManager.HMDMounted += Restart;
        
        OVRManager.TrackingLost += Restart;
    }

    private void OnDisable()
    {
        OVRManager.HMDMounted -= Restart;
        
        OVRManager.TrackingLost -= Restart;
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }



    //public void CloseApp() {
    //    Application.Quit();
    //}

}
