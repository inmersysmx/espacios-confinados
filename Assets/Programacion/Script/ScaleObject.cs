﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleObject : MonoBehaviour
{

    private Transform transformObject;
    // Start is called before the first frame update
    void Start()
    {
        transformObject = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ScaleUp()
    {
        StartCoroutine(UpScale());
    }

    IEnumerator UpScale()
    {
        float t = 1;
        while (t < 1.2f)
        {
            t += Time.deltaTime*0.5f;
            transformObject.localScale = new Vector3(t, t, t);
            yield return null;
        }
    }

    public void ScaleDown()
    {
        StopAllCoroutines();
        if(transformObject.localScale.x >1)
            StartCoroutine(DownScale());
    }

    IEnumerator DownScale()
    {
        float t = transformObject.localScale.x;
        while (t > 1)
        {
            t -= Time.deltaTime * 0.5f;

            transformObject.localScale = new Vector3(t, t, t);
            yield return null;
        }
    }
}
