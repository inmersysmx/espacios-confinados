﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerAtmosfera : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "Hook")
        {
            AdministradorEscenas.instance.tiempoRevisando = 0;
            print("igualando a cero");
            AdministradorEscenas.instance.revisandoAtmosfera = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "Hook")
        {
            AdministradorEscenas.instance.revisandoAtmosfera = false;
        }
    }
}
