﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AdministradorEscenas : Singleton<AdministradorEscenas>
{
    Vector3 initial = new Vector3(0, 1.108f, -18);

    //Escena 0: Candadeo
    public List<GameObject> corazones;
    public GameObject candadoInterior;
    public GameObject botonInteriorConCandado;
    //public GameObject botonInteriorSinCandado;
    public GameObject candadoExterior;
    public GameObject botonExteriorConCandado;
    //public GameObject botonExteriorSinCandado;
    public GameObject botonContinuar;
    public GameObject botonSiYaEntrare;
    public GameObject botonAdelanteVe;
    public GameObject filtroBN;
    public GameObject triggerMedicionExterior;
    public GameObject triggerMedicionInterior;
    public GameObject respuestaCandadeo;
    public GameObject polipastoInterior;
    public GameObject acostadoPersojaneInterior;
    public GameObject sentadoPersojaneInterior;
    public GameObject gasInterior;
    public GameObject polipastoExterior;
    public GameObject acostadoPersojaneExterior;
    public GameObject sentadoPersojaneExterior;
    public GameObject gasExterior;
    public GameObject ultimoCorazon;
    public GameObject tacheCandadeo;
    public GameObject tacheCandadeoVigilante;
    public GameObject tacheVigilanteVigilando;
    public GameObject flechaIzquierda;
    public GameObject flechaDerecha;
    public Text textoReloj;
    public Text textoRelojNumero;
    public RectTransform canvasCandadeo;
    public RectTransform canvasMedicionAtmosfera;
    public RectTransform canvasBtnEntraTanque;
    public RectTransform canvasPedirNoEstarSolo;
    public RectTransform canvasRespuestaEsperar;
    public Animator playerAnimator;
    public Animator faderAnimator;
    public Animator compuertaExterior;
    public Animator compuertaInterior;
    public Animator medidorNumeros;
    public Animator medidorColor;
    public Animator relojAnimator;
    public Animator relojAvanceAnimator;
    public Animator botonGiraIntAnimator;
    public Animator botonGiraExtAnimator;
    public Animator corazonPanel;
    public Animator corazonUI;
    public Button candadoBotonInterior;
    public Button candadoBloqueoInterior;
    public Button candadoBotonExterior;
    public Button candadoBloqueoExterior;
    public bool revisandoAtmosfera;
    public float tiempoRevisando;
    public int manchaslimpiadas;
    public bool bloqueoValvula;
    public bool bloqueoBoton;
    public bool rescateActivo;
    public Renderer medidor;
    public Texture medidorCorrecto;
    bool contador;
    bool atmosferaCorrecta;
    int numeroIntentosCandadeo;
    int indexAnterior;
    float nContador = 60;

    private void Update()
    {
        if(revisandoAtmosfera)
        {
            tiempoRevisando += Time.deltaTime;
            print(tiempoRevisando);
            if(tiempoRevisando > 5)
            {
                revisandoAtmosfera = false;
                triggerMedicionExterior.SetActive(false);
                triggerMedicionInterior.SetActive(false);
                PanelEntrarTanque();
            }
        }
        if(contador)
        {
            if (nContador > 0)
            {
                nContador -= Time.deltaTime;
            }
            else
            {
                ManagerScene1.instance.audioSourcePierdeCorazon.Play();
                contador = false;
                rescateActivo = false;
                nContador = 60;
                ManagerScene1.instance.audioSourceReloj.Stop();
                relojAvanceAnimator.Play("Idle");
                PanelManager.instance.ChangeToNewPanel(26);
            }
            textoRelojNumero.text = ((int)nContador).ToString();
        }
    }

    public void CargaEscenaConFade(int idx)
    {
        StartCoroutine(cargaEscenaConFade(idx));
    }

    IEnumerator cargaEscenaConFade(int idx)
    {
        faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        CargaEscena(idx);
    }

    public void CargaEscena(int idx)
    {
        candadoBloqueoExterior.interactable = false;
        candadoBloqueoInterior.interactable = false;
        candadoBotonExterior.interactable = false;
        candadoBotonInterior.interactable = false;
        ManagerScene1.instance.audioSourceMedidorGas.Stop();
        switch (idx)
        {
            case 0: //Candadeo player
                bloqueoBoton = false;
                bloqueoValvula = false;
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    candadoBloqueoInterior.interactable = true;
                    candadoBotonInterior.interactable = true;
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90f, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(2.237f, 1.108f, -18.255f);
                    canvasCandadeo.SetLeft(277.9f);
                    canvasCandadeo.SetRight(-277.9f);
                    canvasCandadeo.SetTop(-113);
                    canvasCandadeo.SetBottom(113);
                    Vector3 positionCvCn = canvasCandadeo.transform.localPosition;
                    positionCvCn.z = -159.8f;
                    canvasCandadeo.transform.localPosition = positionCvCn;
                    canvasCandadeo.localEulerAngles = new Vector3(0, 90, 0);
                    candadoInterior.SetActive(false);
                    botonGiraIntAnimator.Play("Idle");
                    //botonInteriorConCandado.SetActive(false);
                    //botonInteriorSinCandado.SetActive(true);
                    botonContinuar.SetActive(false);
                }
                else
                {
                    candadoBloqueoExterior.interactable = true;
                    candadoBotonExterior.interactable = true;
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90f, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(1.345f, ManagerScene1.instance.ovrPlaye.transform.position.y, -15.187f);
                    canvasCandadeo.SetLeft(215.3f);
                    canvasCandadeo.SetRight(-215.3f);
                    canvasCandadeo.SetTop(-160);
                    canvasCandadeo.SetBottom(160);
                    Vector3 positionCvCn = canvasCandadeo.transform.localPosition;
                    positionCvCn.z = 37.1f;
                    canvasCandadeo.transform.localPosition = positionCvCn;
                    canvasCandadeo.localEulerAngles = new Vector3(0, 90, 0);
                    candadoExterior.SetActive(false);
                    botonGiraExtAnimator.Play("Idle");
                    //botonExteriorConCandado.SetActive(false);
                    //botonExteriorSinCandado.SetActive(true);
                    botonContinuar.SetActive(false);
                }
                break;
            case 1: //Candadeo vigilante
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    gasInterior.SetActive(false);
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(0, 1.108f, -18);
                }
                else
                {
                    gasExterior.SetActive(false);
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(0, 1.108f, -18);
                }
                break;
            case 2: //Accidente 1
                ManagerScene1.instance.audioSourceMedidorGas.Play();
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    compuertaInterior.Play("abrirInterior");
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(0f, 1.108f, -15.19f);
                    gasInterior.SetActive(true);
                }
                else
                {
                    compuertaExterior.Play("abrirExterior");
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(0f, 1.108f, -15f);
                    gasExterior.SetActive(true);
                }
                break;
            case 3: //Revision Atmosfera
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    if(!atmosferaCorrecta)
                    {
                        atmosferaCorrecta = true;
                        triggerMedicionInterior.SetActive(true);
                    }
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90f, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(2.66f, 1.108f, -15.19f);
                    canvasMedicionAtmosfera.SetLeft(300);
                    canvasMedicionAtmosfera.SetRight(-300);
                    canvasMedicionAtmosfera.SetTop(-0);
                    canvasMedicionAtmosfera.SetBottom(-0);
                    Vector3 positionCvCn = canvasMedicionAtmosfera.transform.localPosition;
                    positionCvCn.z = 140.3f;
                    canvasMedicionAtmosfera.transform.localPosition = positionCvCn;
                    canvasMedicionAtmosfera.localEulerAngles = new Vector3(0, 0f, 0);

                    canvasPedirNoEstarSolo.SetLeft(-85.9f);
                    canvasPedirNoEstarSolo.SetRight(85.9f);
                    canvasPedirNoEstarSolo.SetTop(-12.90003f);
                    canvasPedirNoEstarSolo.SetBottom(12.90003f);
                    positionCvCn = canvasPedirNoEstarSolo.transform.localPosition;
                    positionCvCn.z = -108.3f;
                    canvasPedirNoEstarSolo.transform.localPosition = positionCvCn;
                    canvasPedirNoEstarSolo.localEulerAngles = new Vector3(0, 201.127f, 0);

                    canvasRespuestaEsperar.SetLeft(-85.9f);
                    canvasRespuestaEsperar.SetRight(85.9f);
                    canvasRespuestaEsperar.SetTop(-12.90003f);
                    canvasRespuestaEsperar.SetBottom(12.90003f);
                    positionCvCn = canvasRespuestaEsperar.transform.localPosition;
                    positionCvCn.z = -108.3f;
                    canvasRespuestaEsperar.transform.localPosition = positionCvCn;
                    canvasRespuestaEsperar.localEulerAngles = new Vector3(0, 201.127f, 0);
                }
                else
                {
                    if (!atmosferaCorrecta)
                    {
                        atmosferaCorrecta = true;
                        triggerMedicionExterior.SetActive(true);
                    }
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(2.717f, 1.108f, -15);
                    canvasMedicionAtmosfera.SetLeft(411.1f);
                    canvasMedicionAtmosfera.SetRight(-411.1f);
                    canvasMedicionAtmosfera.SetTop(31.9f);
                    canvasMedicionAtmosfera.SetBottom(-31.9f);
                    Vector3 positionCvCn = canvasMedicionAtmosfera.transform.localPosition;
                    positionCvCn.z = -94.6f;
                    canvasMedicionAtmosfera.transform.localPosition = positionCvCn;
                    canvasMedicionAtmosfera.localEulerAngles = new Vector3(0, 90, 0);
                }
                break;
            case 4: //Boton Entrar al Tanque
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    canvasBtnEntraTanque.SetLeft(242.4f);
                    canvasBtnEntraTanque.SetRight(-242.4f);
                    canvasBtnEntraTanque.SetTop(239.5f);
                    canvasBtnEntraTanque.SetBottom(-239.5f);
                    Vector3 positionCvCn = canvasBtnEntraTanque.transform.localPosition;
                    positionCvCn.z = -40.9f;
                    canvasBtnEntraTanque.transform.localPosition = positionCvCn;
                    canvasBtnEntraTanque.localEulerAngles = new Vector3(90, 90, 0);
                }
                else
                {
                    canvasBtnEntraTanque.SetLeft(416.3f);
                    canvasBtnEntraTanque.SetRight(-416.3f);
                    canvasBtnEntraTanque.SetTop(34.4f);
                    canvasBtnEntraTanque.SetBottom(-34.4f);
                    Vector3 positionCvCn = canvasBtnEntraTanque.transform.localPosition;
                    positionCvCn.z = -40.0f;
                    canvasBtnEntraTanque.transform.localPosition = positionCvCn;
                    canvasBtnEntraTanque.localEulerAngles = new Vector3(0, 90, 0);
                }
                break;
            case 5: //Pedir No estar solo
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90f, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(2.66f, 1.108f, -15.19f);
                }
                else
                {
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 90, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(2.717f, 1.108f, -15);
                }
                break;
            case 6: //Accidente 2
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 51.83f, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(3.7f, -1.8f, -14.6f);
                }
                else
                {
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, -90, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(8.21f, 1.108f, -12.3f);
                }
                break;
            case 7: //Accidente 3
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    acostadoPersojaneInterior.SetActive(true);
                    ManagerScene1.instance.vigilanteInterior.SetActive(false);
                    polipastoInterior.SetActive(true);
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 0, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(1.5f, 1.108f, -16.5f);
                    ManagerScene1.instance.SplashCorazones.transform.Find("MedidorAtmosfera").gameObject.SetActive(false);
                    ManagerScene1.instance.SplashCorazones.transform.Find("Corazones").gameObject.SetActive(false);
                    ultimoCorazon.SetActive(false);
                    ManagerScene1.instance.modeloCepillo.SetActive(false);
                    ManagerScene1.instance.modeloCepilloIz.SetActive(false);
                    ManagerScene1.instance.modeloControlOculus.SetActive(true);
                    ManagerScene1.instance.modeloControlOculusIz.SetActive(true);
                }
                else
                {
                    acostadoPersojaneExterior.SetActive(true);
                    ManagerScene1.instance.vigilanteExterior.SetActive(false);
                    polipastoExterior.SetActive(true);
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 0, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(-1.134f, 1.108f, -16.298f);
                    ManagerScene1.instance.SplashCorazones.transform.Find("MedidorAtmosfera").gameObject.SetActive(false);
                    ManagerScene1.instance.SplashCorazones.transform.Find("Corazones").gameObject.SetActive(false);
                    ultimoCorazon.SetActive(false);
                    ManagerScene1.instance.modeloCepillo.SetActive(false);
                    ManagerScene1.instance.modeloCepilloIz.SetActive(false);
                    ManagerScene1.instance.modeloControlOculus.SetActive(true);
                    ManagerScene1.instance.modeloControlOculusIz.SetActive(true);
                }
                break;
            case 8: //Victoria
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    polipastoInterior.SetActive(false);
                    acostadoPersojaneInterior.SetActive(false);
                    sentadoPersojaneInterior.SetActive(true);
                    //ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 0, 0);
                    //ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(1.5f, 1.108f, -16.5f);
                    //ManagerScene1.instance.SplashCorazones.transform.Find("MedidorAtmosfera").gameObject.SetActive(false);
                    //ManagerScene1.instance.SplashCorazones.transform.Find("Corazones").gameObject.SetActive(false);
                    //ultimoCorazon.SetActive(false);
                    //ManagerScene1.instance.modeloCepillo.SetActive(false);
                    //ManagerScene1.instance.modeloControlOculus.SetActive(true);
                }
                else
                {
                    polipastoExterior.SetActive(false);
                    acostadoPersojaneExterior.SetActive(false);
                    sentadoPersojaneExterior.SetActive(true);
                    //ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 0, 0);
                    //ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(-1.134f, 1.108f, -16.298f);
                    //ManagerScene1.instance.SplashCorazones.transform.Find("MedidorAtmosfera").gameObject.SetActive(false);
                    //ManagerScene1.instance.SplashCorazones.transform.Find("Corazones").gameObject.SetActive(false);
                    //ultimoCorazon.SetActive(false);
                    //ManagerScene1.instance.modeloCepillo.SetActive(false);
                    //ManagerScene1.instance.modeloControlOculus.SetActive(true);
                }
                break;
            case 9: //Actividad limpieza
                if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
                {
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 0f, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(3.7f, -1.8f, -14.6f);
                }
                else
                {
                    ManagerScene1.instance.ovrPlaye.transform.eulerAngles = new Vector3(0, 40, 0);
                    ManagerScene1.instance.ovrPlaye.transform.position = new Vector3(9.41f, 1.108f, -13.074f);
                }
                break;
        }
    }

    public void RevisaIntentosCandadeo(GameObject activado)
    {
        if (activado.name == "BotonTrigger")
            bloqueoBoton = true;
        if (activado.name == "CandadoTrigger")
            bloqueoValvula = true;
        if (numeroIntentosCandadeo < 1)
            botonContinuar.SetActive(true);
        else if(bloqueoBoton && bloqueoValvula)
            botonContinuar.SetActive(true);
    }

    public void RevisionActividadCandadeo()
    {
        if (bloqueoBoton && bloqueoValvula)
        {
            ManagerScene1.instance.audioSourceAcierto.Play();
            if (numeroIntentosCandadeo < 1)
            {
                GameDataManager.instance.candadoValvula = true;
                GameDataManager.instance.candadoBoton = true;
            }
            CargaEscenaConFade(1);
            PanelManager.instance.ChangeToNewPanel(13);
        }
        else
        {
            if(bloqueoBoton)
                GameDataManager.instance.candadoBoton = true;
            else if(bloqueoValvula)
                GameDataManager.instance.candadoValvula = true;
            numeroIntentosCandadeo++;
            //RestaCorazon();
            //Sonido de error
            StartCoroutine(Accidente1(0));
        }
    }

    public void LlamaAccidente1(int idxEscena)
    {
        //RestaCorazon();
        //Sonido de error
        Invoke("DeactivateBotonSiYaEntrare", 2);
        StartCoroutine(Accidente1(idxEscena));
    }

    public void LlamaAccidente2(int idxEscena)
    {
        //RestaCorazon();
        //Sonido de error
        ManagerScene1.instance.audioSourceError.Play();
        Invoke("DeactivateBotonAdelanteVe", 2);
        StartCoroutine(Accidente2(idxEscena));
    }

    void DeactivateBotonSiYaEntrare()
    {
        botonSiYaEntrare.SetActive(false);
    }

    void DeactivateBotonAdelanteVe()
    {
        botonAdelanteVe.SetActive(false);
    }

    IEnumerator Accidente1(int indxRegresar)
    {
        ManagerScene1.instance.audioSourceError.Play();
        yield return new WaitForSeconds(1);
        PanelManager.instance.accidente1 = true;
        PanelManager.instance.ChangeToNewPanel(6); //Panel de accidentes
        faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        CargaEscena(2);
        filtroBN.SetActive(true);
        playerAnimator.enabled = true;
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
        {
            ManagerScene1.instance.vigilanteInterior.GetComponent<EventAnimation>().DesactivaLookAt();
            if (bloqueoBoton && bloqueoValvula)
                playerAnimator.Play("Accidente1Interior2");
            else
                playerAnimator.Play("Accidente1Interior");
        }
        else
        {
            ManagerScene1.instance.vigilanteExterior.GetComponent<EventAnimation>().DesactivaLookAt();
            if (bloqueoBoton && bloqueoValvula)
                playerAnimator.Play("Accidente1Exterior2");
            else
                playerAnimator.Play("Accidente1Exterior");
        }
        yield return new WaitForSeconds(10); //Tiempo que dura la animación
        compuertaExterior.Play("Idle");
        compuertaInterior.Play("Idle");
        faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        CargaEscena(1);
        filtroBN.SetActive(false);
        PanelManager.instance.accidente1 = false;
        PanelManager.instance.ChangeToNewPanel(22);
        indexAnterior = indxRegresar;
        yield return new WaitForSeconds(2);
        RestaCorazon();
        //CargaEscenaAnterior(indxRegresar);
    }

    IEnumerator Accidente2(int indxRegresar)
    {
        GameDataManager.instance.vigilanteVigilando = false;
        yield return new WaitForSeconds(1);
        PanelManager.instance.accidente2 = true;
        PanelManager.instance.ChangeToNewPanel(6); //Panel de accidentes
        faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        CargaEscena(6);
        filtroBN.SetActive(true);
        Vector3 rotationTemp;
        ManagerScene1.instance.audioSourceMedidorPuerta.Play();
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
        {
            ManagerScene1.instance.vigilanteInterior.GetComponent<EventAnimation>().DesactivaLookAt();
            rotationTemp = compuertaInterior.transform.localEulerAngles;
            //compuertaInterior.enabled = true;
            compuertaInterior.Play("cerrarInterior");
        }
        else
        {
            ManagerScene1.instance.vigilanteExterior.GetComponent<EventAnimation>().DesactivaLookAt();
            rotationTemp = compuertaExterior.transform.localEulerAngles;
            //compuertaExterior.enabled = true;
            compuertaExterior.Play("cerrarExterior");
        }
        yield return new WaitForSeconds(7); //Tiempo que dura la animación
        ManagerScene1.instance.audioSourceMedidorPuerta.Stop();
        compuertaExterior.Play("abrirExterior");
        compuertaInterior.Play("abrirInterior");
        faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
        {
            //compuertaInterior.enabled = false;
            compuertaInterior.transform.localEulerAngles = rotationTemp;
        }
        else
        {
            //compuertaExterior.enabled = false;
            compuertaExterior.transform.localEulerAngles = rotationTemp;
        }
        CargaEscena(1);
        filtroBN.SetActive(false);
        yield return new WaitForSeconds(1);
        PanelManager.instance.accidente2 = false;
        PanelManager.instance.ChangeToNewPanel(22);
        indexAnterior = indxRegresar;
        yield return new WaitForSeconds(2);
        RestaCorazon();
        //CargaEscenaAnterior(indxRegresar);
    }

    void RestaCorazon()
    {
        ManagerScene1.instance.audioSourcePierdeCorazon.Play();
        corazones[corazones.Count - 1].GetComponent<Animator>().Play("PierdeCorazon");
        corazonPanel.Play("PierdeCorazon");
        corazonUI.Play("PierdeCorazon");
        //corazones[corazones.Count - 1].SetActive(false);
        corazones.RemoveAt(corazones.Count - 1);
    }

    public void CargaEscenaAnterior()
    {
        Invoke("RegresaCorazonIdle", 2);
        switch (indexAnterior)
        {
            case 0:
                CargaEscenaConFade(0);
                PanelManager.instance.ChangeToNewPanel(12);
                break;
            case 1:
                CargaEscena(1);
                PanelManager.instance.ChangeToNewPanel(13);
                break;
            case 2:
                CargaEscenaConFade(3);
                PanelManager.instance.ChangeToNewPanel(20);
                break;
        }
    }

    void RegresaCorazonIdle()
    {
        corazonPanel.Play("Idle");
        corazonUI.Play("Idle");
    }

    void CargaEscenaAnterior(int idxRegresar)
    {
        switch(idxRegresar)
        {
            case 0:
                CargaEscenaConFade(0);
                PanelManager.instance.ChangeToNewPanel(12);
                break;
            case 1:
                CargaEscenaConFade(1);
                PanelManager.instance.ChangeToNewPanel(13);
                break;
            case 2:
                CargaEscenaConFade(5);
                PanelManager.instance.ChangeToNewPanel(20);
                break;
        }
    }

    public void ActivaAnimacionVigilanteCandadeo()
    {
        ManagerScene1.instance.audioSourceAcierto.Play();
        if (botonSiYaEntrare.activeSelf)
            GameDataManager.instance.vigilanteCandadeo = true;
        StartCoroutine(activaAnimacionVigilanteCandadeo());
    }

    IEnumerator activaAnimacionVigilanteCandadeo()
    {
        yield return new WaitForSeconds(4);
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
            ManagerScene1.instance.vigilanteInterior.GetComponent<EventAnimation>().DesactivaLookAt();
        else
            ManagerScene1.instance.vigilanteExterior.GetComponent<EventAnimation>().DesactivaLookAt();
        yield return new WaitForSeconds(1);
        respuestaCandadeo.SetActive(false);
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
        {
            ManagerScene1.instance.vigilanteInterior.GetComponent<EventAnimation>().DesactivaLookAt();
            ManagerScene1.instance.vigilanteInterior.GetComponent<Animator>().SetTrigger("candadeo");
        }
        else
        {
            ManagerScene1.instance.vigilanteExterior.GetComponent<EventAnimation>().DesactivaLookAt();
            ManagerScene1.instance.vigilanteExterior.GetComponent<Animator>().SetTrigger("candadeo");
        }
        yield return new WaitForSeconds(15);
        PanelManager.instance.ChangeToNewPanel(15);
    }

    public void ActivaAnimacionRevisandoVacio()
    {
        StartCoroutine(activaAnimacionRevisandoVacio());
    }

    IEnumerator activaAnimacionRevisandoVacio()
    {
        faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        //ManagerScene1.instance.ovrPlaye.GetComponent<OVRPlayerController>().enabled = false;
        playerAnimator.enabled = true;
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
        {
            compuertaInterior.Play("abrirInterior");
            playerAnimator.Play("GoToEnterInterior");
        }
        else
        {
            compuertaExterior.Play("abrirExterior");
            playerAnimator.Play("GoToEnterExterior");
        }
        yield return new WaitForSeconds(10);
        //ManagerScene1.instance.ovrPlaye.GetComponent<OVRPlayerController>().enabled = false;
        CargaEscenaConFade(1);
        //playerAnimator.enabled = true;
        //if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
        //    playerAnimator.Play("ReturnInterior");
        //else
        //    playerAnimator.Play("ReturnExterior");
        yield return new WaitForSeconds(3);
        PanelManager.instance.ChangeToNewPanel(17);
    }

    public void PanelEntrarTanque()
    {
        medidor.material.SetTexture("_MainTex", medidorCorrecto);
        ManagerScene1.instance.audioSourceAcierto.Play();
        CargaEscena(4);
        PanelManager.instance.ChangeToNewPanel(19);
    }

    public void EntrarTanque()
    {
        StartCoroutine(entrarTanque());
    }

    IEnumerator entrarTanque()
    {
        yield return new WaitForSeconds(1);
        PanelManager.instance.ChangeToNewPanel(20);
    }

    public void EsperaYEntra()
    {
        StartCoroutine(esperaYEntra());
    }

    IEnumerator esperaYEntra()      //Actividad de limpieza
    {
        ManagerScene1.instance.audioSourceAcierto.Play();
        PanelManager.instance.limpiando = true;
        yield return new WaitForSeconds(3);
        ManagerScene1.instance.modeloMedidorAtmosferico.SetActive(false);
        ManagerScene1.instance.modeloCepillo.SetActive(true);
        ManagerScene1.instance.modeloControlOculusIz.SetActive(false);
        ManagerScene1.instance.modeloCepilloIz.SetActive(true);
        PanelManager.instance.ChangeToNewPanel(6);
        CargaEscenaConFade(9);
        yield return new WaitForSeconds(1);
        //Suena audio de limpieza
        while (manchaslimpiadas < 1)
            yield return null;
        medidorColor.Play("SubeColor");
        medidorNumeros.Play("Subenumeros");
        ManagerScene1.instance.audioSourceMedidorAtmosfera.Play();
        yield return new WaitForSeconds(12);
        ManagerScene1.instance.ChangeAmbientalAudio(1);
        ManagerScene1.instance.audioSourceMedidorGas.Play();
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
        {
            gasInterior.SetActive(true);
        }
        else
        {
            gasExterior.SetActive(true);
        }
        yield return new WaitForSeconds(6);
        ultimoCorazon.SetActive(true);
        RestaCorazon();
        yield return new WaitForSeconds(2);
        ManagerScene1.instance.audioSourceMedidorAtmosfera.Stop();
        faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        RegresaCorazonIdle();
        PanelManager.instance.ChangeToNewPanel(23);
        CargaEscena(7);
        yield return new WaitForSeconds(7);
        PanelManager.instance.ChangeToNewPanel(24);
        flechaIzquierda.SetActive(true);
        flechaDerecha.SetActive(true);
    }

    public void StartRescate()
    {
        StartCoroutine(Rescate());
    }

    IEnumerator Rescate()
    {
        flechaIzquierda.SetActive(false);
        flechaDerecha.SetActive(false);
        PanelManager.instance.ChangeToNewPanel(25);
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
        {
            relojAnimator.transform.localPosition = new Vector3(74.8f, 30.9f, -45.9f);
            textoReloj.transform.localPosition = new Vector3(74.8f, 2.72f, -45.9f);
        }
        else
        {
            relojAnimator.transform.localPosition = new Vector3(-82.88f, 20.72f, -30.2f);
            textoReloj.transform.localPosition = new Vector3(-82.88f, -4.7f, -30f);
        }
        relojAnimator.gameObject.SetActive(true);
        yield return new WaitForSeconds(1);
        textoReloj.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        textoReloj.gameObject.SetActive(false);
        //relojAnimator.Play("Baja");
        yield return new WaitForSeconds(1);
        ManagerScene1.instance.audioSourceReloj.Play();
        relojAvanceAnimator.Play("Avanza");
        contador = true;
        rescateActivo = true;
        //Corre tiempo y cuenta vueltas
    }

    public void RepiteRescate()
    {
        ManagerScene1.instance.audioSourceReloj.Play();
        relojAvanceAnimator.Play("Avanza");
        contador = true;
        rescateActivo = true;
    }

    public void Victoria()
    {
        ManagerScene1.instance.audioSourceReloj.Stop();
        relojAvanceAnimator.Play("Idle");
        contador = false;
        rescateActivo = false;
        ManagerScene1.instance.audioSourceAcierto.Play();
        StartCoroutine(victoria());
    }

    IEnumerator victoria()
    {
        faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        ManagerScene1.instance.ChangeAmbientalAudio(0);
        PanelManager.instance.ChangeToNewPanel(27);
        CargaEscena(8);
    }

    public void Final()
    {
        StartCoroutine(final());        
    }

    IEnumerator final()
    {
        faderAnimator.SetTrigger("fade");
        yield return new WaitForSeconds(1);
        sentadoPersojaneExterior.SetActive(false);
        sentadoPersojaneInterior.SetActive(false);
    }

    public void CalculaReporte()
    {
        if(ManagerScene1.instance.audioSourceAmbiental.clip.name != ManagerScene1.instance.audiosClipsAmbiental[0].name)
            ManagerScene1.instance.ChangeAmbientalAudio(0);
        if (!GameDataManager.instance.candadoBoton || !GameDataManager.instance.candadoValvula)
            tacheCandadeo.SetActive(true);
        if (!GameDataManager.instance.vigilanteCandadeo)
            tacheCandadeoVigilante.SetActive(true);
        if (!GameDataManager.instance.vigilanteVigilando)
            tacheVigilanteVigilando.SetActive(true);
        GameDataManager.instance.StoreInformation();
    }

    public void AnimBotonUser(string anim)
    {
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
            botonGiraIntAnimator.Play(anim);
        else
            botonGiraExtAnimator.Play(anim);
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
