﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextInputManager : MonoBehaviour
{
    public Text[] textInputs;
    public Text currentTextInput;

    public bool limitDigitsOn;
    public int numberOfCharacters;

    public GameObject continuar1;
    public GameObject continuar2;

    public 

    // Start is called before the first frame update
    void Start()
    {
        currentTextInput = textInputs[1];
        EraseAll();
        limitDigitsOn = true;
        numberOfCharacters = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddLetter(string letter)
    {
        if (!limitDigitsOn)
        {
            currentTextInput.text = currentTextInput.text + letter;
        }
        else
        {
            if (numberOfCharacters < 8)
                currentTextInput.text = currentTextInput.text + letter;
            else
                numberOfCharacters--;
        }

        numberOfCharacters++;

        continuar1.SetActive(true);
        continuar2.SetActive(true);

    }

    public void AddSpace()
    {
        currentTextInput.text = currentTextInput.text + " ";
    }

    public void ResetTextCount()
    {
        numberOfCharacters = 0;
        continuar1.SetActive(false);
        continuar2.SetActive(false);
    }

    public void LimitActivation(bool active)
    {
        limitDigitsOn = active;
    }

    public void EraseAll()
    {
        currentTextInput.text = "";
        ResetTextCount();
        continuar1.SetActive(false);
        continuar2.SetActive(false);
    }

    public void DeleteOne()
    {
        if (numberOfCharacters != 0 )
        {
            string value = currentTextInput.text;
            value = value.Substring(0, value.Length - 1);
            currentTextInput.text = value;
            numberOfCharacters--;
        }



        if (numberOfCharacters == 0)
        {
            continuar1.SetActive(false);
            continuar2.SetActive(false);
        }
    }

    public void ChangeInputIndex(int index)
    {
        currentTextInput = textInputs[index];
        EraseAll();
    }
}
