﻿using UnityEngine;

public class TriggerVueltaManivela : MonoBehaviour
{
    public Transform cuerda;

    Vector3 initialCuerda;
    bool terminado;

    private void Start()
    {
        print(Application.persistentDataPath);
        initialCuerda = cuerda.transform.localScale;
    }

    void Avance(GameObject other)
    {
        ActividadRescatar.instance.avance++;
        ActividadRescatar.instance.prevTrigger = other.gameObject;
        cuerda.localScale = new Vector3(cuerda.localScale.x, cuerda.localScale.y - 0.02f, cuerda.localScale.z);
        if (ManagerScene1.instance.curScenario.name == "Escenario Interior")
        {
            if (cuerda.localScale.y <= 0.8f && !terminado)
            {
                terminado = true;
                AdministradorEscenas.instance.Victoria();
            }
        }
        else
        {
            if (cuerda.localScale.y <= 1.4f && !terminado)
            {
                terminado = true;
                AdministradorEscenas.instance.Victoria();
            }
        }
        print(cuerda.localScale.y + ", " + terminado);
    }

    private void OnTriggerEnter(Collider other)
    {
        switch(other.name)
        {
            case "Primero":
                if (ActividadRescatar.instance.prevTrigger.name == "Cuarto")
                {
                    Avance(other.gameObject);
                }
                break;
            case "Segundo":
                if (ActividadRescatar.instance.prevTrigger.name == "Primero")
                {
                    Avance(other.gameObject);
                }
                break;
            case "Tercero":
                if (ActividadRescatar.instance.prevTrigger.name == "Segundo")
                {
                    Avance(other.gameObject);
                }
                break;
            case "Cuarto":
                if(ActividadRescatar.instance.prevTrigger==null)
                    ActividadRescatar.instance.prevTrigger = other.gameObject;
                if (ActividadRescatar.instance.prevTrigger.name == "Tercero")
                {
                    Avance(other.gameObject);
                }
                break;
        }
    }
}
