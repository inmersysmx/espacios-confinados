﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManivelaLookAt : MonoBehaviour
{
    Vector3 offset;

    private void Start()
    {
        offset = ActividadRescatar.instance.targetManivela.transform.position - transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        offset = ActividadRescatar.instance.targetManivela.transform.position - transform.position;
        transform.rotation = Quaternion.LookRotation(
                           -Vector3.forward, // Keep z+ pointing straight into the screen.
                           offset           // Point y+ toward the target.
                         );
    }
}