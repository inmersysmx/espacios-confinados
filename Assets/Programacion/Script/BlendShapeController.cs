﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendShapeController : MonoBehaviour
{

    public SkinnedMeshRenderer skinnedMeshRenderer;
    Mesh skinnedMesh;
    float blendOne = 100f;
    float blendTwo = 100f;
    float blendThree = 100f;
    float blendSpeed = 1f;

    
    void Start()
    {
        //skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void tightharness(int index)
    {
        StartCoroutine(HarnessTightener(index));
    }

    IEnumerator HarnessTightener(int index)
    {
        float t = 100f;
        while (t > 0)
        {
            t -= Time.deltaTime * 100;
            skinnedMeshRenderer.SetBlendShapeWeight(index, t);
            yield return null;
        }
        
    }
}
