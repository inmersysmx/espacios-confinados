﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Giro : MonoBehaviour
{

    [Header("CONFIGURACION")]
    public float velocidad = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        girar();   
    }



    void girar()
    {
        this.transform.Rotate(Vector3.up*Time.deltaTime*velocidad);

    }

}
