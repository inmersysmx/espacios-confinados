﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDataManager : MonoBehaviour {

    public static GameDataManager instance;

    [Header("Valores que se ocupan de logica")]
    public int locationNumber;
    public bool inspeccionArnes;
    public bool inspeccionLinea;
    public bool perfectScore;

    [Header("Valores que se guardaran")]
    public string id;
    public string userName;
    //public string location;
    //public bool selectHarness;
    public bool quemadura1;
    public bool rotura;
    public bool oxido1;
    public bool brocheRoto;
    public bool etiqueta1;
    public bool ajuste;
    //public bool selectLifeline;
    public bool desviacion;
    public bool oxido2;
    public bool ganchoAbierto;
    public bool dano;
    public bool quemadura2;
    public bool etiqueta2;
    public bool insidcadorRojo;
    public bool anchoreLine;

    public bool candadoValvula;
    public bool candadoBoton;
    public bool vigilanteCandadeo;
    public bool vigilanteVigilando;

    //public bool isTestedDesviacion;
    public bool isTestedDano;
    public bool isTestedQuemadura2;
    public bool isTestedEtiqueta2;
    public bool isTestedInsidcadorRojo;


    void Awake() {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        
        
    }

    public void StoreInformation()
    {
        CSVManager.AppendToReport(GetReportLine());
    }

    string[] GetReportLine() {
        string[] returnable = new string[18];
        returnable[0] = id;
        returnable[1] = userName;
        returnable[2] = CorrectoIncorrecto(quemadura1);
        returnable[3] = CorrectoIncorrecto(rotura);
        returnable[4] = CorrectoIncorrecto(oxido1);
        returnable[5] = CorrectoIncorrecto(brocheRoto);
        returnable[6] = CorrectoIncorrecto(etiqueta1);
        returnable[7] = CorrectoIncorrecto(ajuste);
        returnable[8] = CorrectoIncorrecto(desviacion);
        returnable[9] = CorrectoIncorrecto(oxido2);
        returnable[10] = CorrectoIncorrecto(ganchoAbierto);
        returnable[11] = CorrectoIncorrecto(dano);
        returnable[12] = CorrectoIncorrecto(quemadura2);
        returnable[13] = CorrectoIncorrecto(etiqueta2);
        returnable[14] = CorrectoIncorrecto(candadoValvula);
        returnable[15] = CorrectoIncorrecto(candadoBoton);
        returnable[16] = CorrectoIncorrecto(vigilanteCandadeo);
        returnable[17] = CorrectoIncorrecto(vigilanteVigilando);
        return returnable;
    }

    public string CorrectoIncorrecto(bool correcto)
    {
        if (correcto)
            return "Correcto";
        else
            return "Incorrecto";
    }

    public string CorrectIncorrectTested(bool correcto, bool isTested)
    {
        if (isTested)
            return CorrectoIncorrecto(correcto);
        else
            return "No aplica";
    }

    public bool CheckEverythingCorrect()
    {
        perfectScore = false;

        if(CheckInspeccionArnes() && CheckInpeccionLinea() && anchoreLine)
            perfectScore = true;

        return perfectScore;
    }

    public bool CheckInspeccionArnes()
    {
        inspeccionArnes = false;

        if (quemadura1 && rotura && oxido1 && brocheRoto && etiqueta1 && ajuste)
            inspeccionArnes = true;

        return inspeccionArnes;
    }

    public bool CheckInpeccionLinea()
    {
        inspeccionLinea = false;

        if (desviacion && oxido2 && ganchoAbierto)
            inspeccionLinea = true;

        if(!dano && isTestedDano)
            inspeccionLinea = false;

        if (!quemadura2 && isTestedQuemadura2)
            inspeccionLinea = false;

        if (!etiqueta2 && isTestedEtiqueta2)
            inspeccionLinea = false;

        if (!insidcadorRojo && isTestedInsidcadorRojo)
            inspeccionLinea = false;

        return inspeccionLinea;
    }


    public void ResetAllValues()
    {
        locationNumber = 0;
        inspeccionArnes = false;
        inspeccionLinea = false;
        perfectScore = false;
        
        id = " ";
        userName = " ";
        quemadura1 = false;
        rotura = false;
        oxido1 = false;
        brocheRoto = false;
        etiqueta1 = false;
        ajuste = false;
        desviacion = false;
        oxido2 = false;
        ganchoAbierto = false;
        dano = false;
        quemadura2 = false;
        etiqueta2 = false;
        insidcadorRojo = false;
        anchoreLine = false;

        candadoValvula = false;
        candadoBoton = false;
        vigilanteCandadeo = false;
        vigilanteVigilando = true;

        isTestedDano = false;
        isTestedQuemadura2 = false;
        isTestedEtiqueta2 = false;
        isTestedInsidcadorRojo = false;
    }

}
